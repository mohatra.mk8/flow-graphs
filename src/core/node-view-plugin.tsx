import { makeObservable, observable } from 'mobx'
import { INode } from './types/node'

export type NodeViewPluginCreator = <T extends INodeViewPlugin>(
  node: INode<any>,
) => T

export abstract class INodeViewPlugin {
  @observable
  node: INode<any>

  constructor(node: INode<any>) {
    this.node = node
    makeObservable(this)
  }

  abstract render(): JSX.Element
}
