import { action } from 'mobx'
import { observer } from 'mobx-react-lite'
import { FaStickyNote } from 'react-icons/fa'
import { NodeDefination, registerNode } from '../node-registry'
import { INodeViewPlugin } from '../node-view-plugin'
import { Action, INode } from '../types/node'
import { InputPort, OutputPort } from '../types/port'
import './text-viewer.scss'

export class TextViewer extends INode<undefined> {
  constructor(id: number) {
    super(id, 'text-viewer', 'full-body')
    this.initialize()
  }

  initialize = () => {
    const input = new InputPort(this, '', 'array')
    input.value = []
    this.inputs = [input]
    this.nodeViewPlugins.set('preview', new TextViewerPlugin(this))
  }

  @action reduceState = (
    action: Action,
  ): { outputs: OutputPort[]; state: any } => {
    console.log('Text viewer state: ', action, this.inputs[0])
    return { outputs: this.outputs, state: this.state }
  }
}

class TextViewerPlugin extends INodeViewPlugin {
  render = () => {
    return <TextViewerObserver values={this.node.inputs[0].value} />
  }
}

const TextViewerObserver = observer(({ values }: any) => {
  console.log('Input is', values)
  return (
    <div style={{ minWidth: 200 }}>
      {values.map((text: any, index: number) => {
        if (text instanceof String || typeof text === 'string') {
          return (
            <div className="text-viewer" key={'text' + index}>
              <p>{text}</p>
            </div>
          )
        }
        return <div>{text}</div>
      })}
    </div>
  )
})

registerNode(
  new NodeDefination(
    'text-viewer',
    ['core'],
    <FaStickyNote />,
    (id: number) => new TextViewer(id),
    'Simple text viewer',
  ),
)
