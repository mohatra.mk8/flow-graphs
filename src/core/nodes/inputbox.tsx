import { action } from 'mobx'
import { observer } from 'mobx-react-lite'
import React from 'react'
import { FaTerminal } from 'react-icons/fa'
import { NodeDefination, registerNode } from '../node-registry'
import { INodeViewPlugin } from '../node-view-plugin'
import { Actions } from '../types/flow'
import { Action, INode } from '../types/node'
import { OutputPort } from '../types/port'
import './inputbox.scss'

export class InputBox extends INode<string> {
  constructor(id: number) {
    super(id, 'text-input', 'header-only')
    this.initialize()
    console.log('Id created', id)
  }

  @action
  initialize = () => {
    this.state = ''
    this.nodeViewPlugins.set('preview', new InputBoxViewPlugin(this))
    this.propagateActionToFlow({ type: Actions.NODE_INITIALIZATION })
  }

  @action
  reduceState = (action: Action): { outputs: OutputPort[]; state: any } => {
    if (action.type === Actions.NODE_INITIALIZATION) {
      this.state = ''
      const op = new OutputPort(this, '', 'single')
      op.value = this.state
      return { outputs: [op], state: '' }
    } else if (action.type === 'VALUE_CHANGED') {
      const value = action.value

      const op = new OutputPort(this, '', 'single')
      return { outputs: [op], state: value }
    }
    return { outputs: this.outputs, state: this.state }
  }
}

class InputBoxViewPlugin extends INodeViewPlugin {
  render = () => {
    return <InputBoxObserver node={this.node} />
  }
}

const InputBoxObserver = observer(({ node }: { node: INode<string> }) => {
  return (
    <div style={{ padding: 5, paddingLeft: 0 }}>
      <input
        type="text"
        value={node.state}
        placeholder="Enter text..."
        style={{
          outline: 'none',
          padding: 5,
          border: 'none',
          borderRadius: '4px',
          maxWidth: 150,
        }}
        onChange={(e) => {
          node.propagateActionToFlow({
            type: 'VALUE_CHANGED',
            value: e.target.value,
          })
        }}
      />
    </div>
  )
})

registerNode(
  new NodeDefination(
    'text field',
    ['core', 'input'],
    <FaTerminal color={'green'} />,

    (id: number) => new InputBox(id),
    'Simple text field',
  ),
)
