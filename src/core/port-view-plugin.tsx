import { observable } from 'mobx'
import { IPort } from './types/port'

export type PortViewPluginCreator = <T extends IPortViewPlugin>(
  port: IPort,
) => T

export abstract class IPortViewPlugin {
  @observable
  port: IPort

  constructor(port: IPort) {
    this.port = port
  }

  abstract render(): JSX.Element
}
