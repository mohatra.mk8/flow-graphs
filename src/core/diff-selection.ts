
export type KeyFunction<T> = (t: T) => any
export type Predicate<T> = (...values: T[]) => boolean

export class Diff<T >{

    private enter: T[];
    private exit: T[];
    private update: T[];
    private same: T[];

    entered = ()=>{
        return [...this.enter]
    }

    updated = ()=>{
        return [...this.update]
    }

    exited = ()=>{
        return [...this.exit]
    }

    noChange = ()=>{
        return [...this.same]
    }

    all = ()=>{
        return [...this.enter, ...this.update, ...this.exit, ...this.same]
    }

    present = ()=>{
        return [...this.enter, ...this.update, ...this.same]
    }

    private constructor(enter: T[], update: T[], exit: T[], same: T[]){
        this.enter = enter
        this.update = update
        this.exit = exit
        this.same = same
    }

    public static createDiff<T>(oldList: T[], newList:T[], keyFn: KeyFunction<T>, comparator: Predicate<T> = (a,b)=>a===b){
        const entered = newList.filter(nt=>{
            return !oldList.some(ot=>keyFn(ot)=== keyFn(nt))
        })

        const exited =  oldList.filter(ot=>{
            return !newList.some(nt=> keyFn(ot)=== keyFn(nt))
        })

        const updated = oldList.filter(ot=>{
            return newList.some(nt=> keyFn(nt)=== keyFn(ot) && !comparator(nt, ot))
        })

        const same =  oldList.filter(ot=>{
            return newList.some(nt=> keyFn(nt) === keyFn(ot) && comparator(nt, ot))
        })

        return new Diff<T>(entered, updated, exited, same)
    }
}