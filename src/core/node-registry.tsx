import { NodeCreator } from './types/node'

const nodesList = new Map<string, NodeDefination>()

const KEY_DELIMITER = '!'
const getNodeKey = ({ group, name }: { group: string[]; name: string }) => {
  return [...group, name].reduce((acc, ki) => {
    return acc + KEY_DELIMITER + ki
  })
}

const createNodesTree = (nodes: NodeDefination[]) => {
  const nodesTree: NodesTree = {}
  nodes.forEach((n) => {
    const group = n.group
    let innerTree = nodesTree
    for (let g of group) {
      if (!(g in innerTree)) {
        innerTree[g] = {} as NodesTree
      }
      innerTree = innerTree[g] as NodesTree
    }
    innerTree[n.name] = n
  })

  return nodesTree
}

export class NodeDefination {
  name: string
  group: string[]
  icon: JSX.Element

  toolTip: JSX.Element | string

  nodeCreator: NodeCreator

  constructor(
    name: string,
    group: string[],
    icon: JSX.Element,
    nodeCreator: any,
    tooltip: any,
  ) {
    this.name = name
    this.group = group
    this.icon = icon
    this.nodeCreator = nodeCreator
    this.toolTip = tooltip
  }
}

export interface NodesTree {
  [key: string]: NodesTree | NodeDefination
}

export const registerNode = (node: NodeDefination) => {
  const nodeKey = getNodeKey(node)
  if (nodesList.has(nodeKey)) {
    throw new Error()
  }
  nodesList.set(nodeKey, node)
}

export const searchNodes = (sf: string) => {
  const keys = Array.from(nodesList.keys()).filter((k) =>
    k.toUpperCase().includes(sf.toUpperCase()),
  )
  return createNodesTree(keys.map((k) => nodesList.get(k)!))
}

export const getNodeDefination = ({ name, group }: any) => {
  const nodeKey = getNodeKey({ name, group })
  return nodesList.get(nodeKey)
}
