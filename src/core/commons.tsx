import { Vector2d, Vector3d } from './data-structure'

export const SVG_XML_NS = 'http://www.w3.org/2000/svg'

export function clientToRelativePosition(
  { clientX, clientY }: { clientX: number; clientY: number },
  relativeTo: HTMLElement,
) {
  const elementRect = relativeTo.getBoundingClientRect()
  return new Vector2d(clientX - elementRect.left, clientY - elementRect.top)
}

export function getMatrixTranslateValues(element: HTMLElement) {
  try {
    const style: any = window.getComputedStyle(element)
    const matrix =
      style.transform || style.webkitTransform || style.mozTransform

    const matrixType = matrix.includes('3d') ? '3d' : '2d'
    const matrixValues = matrix.match(/matrix.*\((.+)\)/)[1].split(', ')
    if (matrixType === '2d') {
      const x = parseFloat(matrixValues[4])
      const y = parseFloat(matrixValues[5])
      return new Vector2d(x, y)
    } else {
      var x = parseFloat(matrixValues[12])
      var y = parseFloat(matrixValues[13])
      var z = parseFloat(matrixValues[14])
      return new Vector3d(x, y, z)
    }
  } catch (error) {
    return new Vector2d()
  }
}

export function cubicBezierPath(a0: Vector2d, a3: Vector2d) {
  const delX = Math.abs(a3.x - a0.x)

  let anchor: number
  if (delX <= 0) {
    anchor = Math.max(Math.abs(delX) / 2, 200)
  } else {
    anchor = Math.max(Math.abs(delX / 2), 100)
  }
  const a2 = { x: a3.x - Math.abs(anchor), y: a3.y }
  const a1 = { x: a0.x + Math.abs(anchor), y: a0.y }

  return `M ${a0.x} ${a0.y} C ${a1.x} ${a1.y}, ${a2.x} ${a2.y}, ${a3.x} ${a3.y}`
}

export function getElementOffset(element: HTMLElement) {
  const rect = element.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop
  return {
    top: rect.top + scrollTop,
    left: rect.left + scrollLeft,
  }
}

export function pageToRelativePosition(
  { pageX, pageY }: { pageX: number; pageY: number },
  element: HTMLElement,
): Vector2d {
  const elementOffset = getElementOffset(element)
  return new Vector2d(pageX - elementOffset.left, pageY - elementOffset.top)
}

export const getPathBetweenTwoElement = (
  from: HTMLElement,
  to: HTMLElement,
  relativeTo: HTMLElement,
) => {
  const fromRefDOM = from.getBoundingClientRect()
  const fromRefPagePosition = getElementOffset(from)
  const startPosition = pageToRelativePosition(
    {
      pageX: fromRefPagePosition.left + fromRefDOM.width / 2,
      pageY: fromRefPagePosition.top + fromRefDOM.height / 2,
    },
    relativeTo,
  )

  const toRefDOM = to.getBoundingClientRect()
  const toRefPagePosition = getElementOffset(to)
  const endPosition = pageToRelativePosition(
    {
      pageX: toRefPagePosition.left + toRefDOM.width / 2,
      pageY: toRefPagePosition.top + toRefDOM.height / 2,
    },
    relativeTo,
  )
  return cubicBezierPath(startPosition, endPosition)
}
