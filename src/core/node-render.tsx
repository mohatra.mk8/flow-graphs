import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import React from 'react'
import Draggable from 'react-draggable'
import { FaHandRock } from 'react-icons/fa'
import { Vector2d } from './data-structure'
import './node-render.scss'
import flow from './types/flow'
import { INode } from './types/node'
import { IPort } from './types/port'

export const NodeRender = observer(({ node }: { node: INode<any> }) => {
  return (
    <Draggable
      handle={'.node-grip'}
      position={node.position}
      onStop={(event, position) => {
        node.position = new Vector2d(position.x, position.y)
      }}
    >
      <div
        className={classNames(
          'graph-node fg-card',
          node.selected ? 'selected' : 'not-selected',
        )}
        onClick={(event) => {
          node.selectMe(event.ctrlKey)
        }}
      >
        {node.bodyType === 'full-body' && (
          <div className={'node-header node-grip fg-card-header'}>
            {node.name}
          </div>
        )}
        <div className={'graph-node-body'}>
          <InputPorts node={node} />
          <PreviewRender node={node} />
          {node.bodyType === 'header-only' && (
            <span
              style={{ minWidth: 20, display: 'flex', alignItems: 'center' }}
              className="node-grip"
            >
              <FaHandRock color={'orange'} />
            </span>
          )}
          <OutputPorts node={node} />
        </div>
      </div>
    </Draggable>
  )
})

const PreviewRender = observer(({ node }: { node: INode<any> }) => {
  const preview = node.nodeViewPlugins.get('preview')
  return <div className="node-preview">{preview && preview.render()}</div>
})

const InputPorts = observer(({ node }: { node: INode<any> }) => {
  const ports = node.inputs
  console.log('Input ports', ports)
  return (
    <div className="input-port-container">
      {ports.map((port) => {
        return <InputPortRender key={port.id()} port={port} />
      })}
    </div>
  )
})

const InputPortRender = observer(({ port }: { port: IPort }) => {
  return (
    <div className={classNames('port-row', port.type)}>
      <div
        className={classNames('endpoint input', port.structure)}
        ref={(element) => {
          element && flow.registerPort(port, element)
        }}
      ></div>
      {port.viewPlugin ? port.viewPlugin.render() : <span>{port.name}</span>}
    </div>
  )
})

const OutputPorts = observer(({ node }: { node: INode<any> }) => {
  const ports = node.outputs
  return (
    <div className="output-port-container">
      {ports.map((port) => {
        return <OutputPortRender key={port.id()} port={port} />
      })}
    </div>
  )
})

const OutputPortRender = observer(({ port }: { port: IPort }) => {
  return (
    <div className={classNames('port-row', port.type)}>
      {port.viewPlugin ? port.viewPlugin.render() : <span>{port.name}</span>}
      <div
        className={classNames('endpoint output', port.structure)}
        ref={(element) => {
          element && flow.registerPort(port, element)
        }}
      ></div>
    </div>
  )
})
