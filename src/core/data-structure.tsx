import { Identifiable, Identifier } from './types/identifiable'

export class OneToOneMap<K, V> {
  private forwardReference: Map<any, V>
  private backwardReference: Map<any, K>

  private keyChanger: (key: K) => any
  private valueChanger: (value: V) => any

  constructor(
    keyChanger: (key: K) => any = (k) => k,
    valueChanger: (value: V) => any = (v) => v,
  ) {
    this.keyChanger = keyChanger
    this.valueChanger = valueChanger

    this.backwardReference = new Map()
    this.forwardReference = new Map()
  }

  keys = () => {
    return this.backwardReference.values()
  }

  values = () => {
    return this.forwardReference.values()
  }

  set = (key: K, value: V) => {
    this.put(key, value)
  }

  put = (key: K, value: V) => {
    const ck = this.keyChanger(key)
    const cv = this.valueChanger(value)

    this.forwardReference.delete(ck)
    this.backwardReference.delete(cv)

    this.forwardReference.set(ck, value)
    this.backwardReference.set(cv, key)
  }

  get = (key: K) => {
    const ck = this.keyChanger(key)
    return this.forwardReference.get(ck)
  }

  getKeyByValue = (value: V) => {
    const cv = this.valueChanger(value)
    return this.backwardReference.get(cv)
  }

  clear = () => {
    this.forwardReference.clear()
    this.backwardReference.clear()
  }

  delete = (key: K) => {
    const ck = this.keyChanger(key)
    const value = this.get(key)
    if (value) {
      const cv = this.valueChanger(value)
      this.backwardReference.delete(cv)
    }
    this.forwardReference.delete(ck)
  }

  deleteByValue = (value: V) => {
    const cv = this.valueChanger(value)
    const key = this.getKeyByValue(value)
    if (key) {
      const ck = this.keyChanger(key)
      this.forwardReference.delete(ck)
    }
    this.backwardReference.delete(cv)
  }
}

export class IdentifiableMap<K extends Identifiable, V> {
  private keyReference: Map<Identifier, K>
  private valueReference: Map<Identifier, V>

  constructor() {
    this.keyReference = new Map()
    this.valueReference = new Map()
  }

  set = (key: K, value: V) => {
    this.put(key, value)
  }

  put = (key: K, value: V) => {
    const ck = key.id()

    this.valueReference.set(ck, value)
    this.keyReference.set(ck, key)
  }

  get = (key: K) => {
    const ck = key.id()
    return this.valueReference.get(ck)
  }

  keys = () => {
    return this.keyReference.values()
  }

  values = () => {
    return this.valueReference.values()
  }
}

export class IdentifiableSet<K extends Identifiable> {
  private collector: IdentifiableMap<K, K>

  constructor() {
    this.collector = new IdentifiableMap()
  }

  push = (k: K) => {
    this.collector.put(k, k)
  }

  forEach = (consumer: (k: K) => void) => {
    this.values().forEach(consumer)
  }

  map = (mapper: (k: K) => any) => {
    return this.values().map(mapper)
  }

  filter = (filterFunction: (k: K) => boolean) => {
    return this.values().filter(filterFunction)
  }

  values = () => {
    return Array.from(this.collector.values())
  }

  some = (filterFunction: (k: K) => boolean) => {
    return this.values().some(filterFunction)
  }

  static from<T extends Identifiable>(list: T[]) {
    const set = new IdentifiableSet<T>()
    list.forEach((l) => {
      set.push(l)
    })
    return set
  }
}

export class Vector3d {
  x: number
  y: number
  z: number
  constructor(x = 0, y = 0, z = 0) {
    this.x = x
    this.y = y
    this.z = z
  }
}

export class Vector2d {
  x: number
  y: number

  top: number
  left: number

  constructor(x = 0, y = 0) {
    this.x = x
    this.y = y
    this.top = y
    this.left = x
  }

  static distance(vec1: Vector2d, vec2: Vector2d) {
    return Math.sqrt(
      (vec1.x - vec2.x) * (vec1.x - vec2.x) +
        (vec1.y - vec2.y) * (vec1.y - vec2.y),
    )
  }
}
