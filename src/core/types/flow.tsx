import { action, makeObservable, observable } from 'mobx'
import { IdentifiableSet, OneToOneMap, Vector2d } from '../data-structure'
import { Diff } from '../diff-selection'
import { NodeDefination } from '../node-registry'
import { Connection } from './connection'
import { Action, INode } from './node'
import {
  InputPort,
  IPort,
  OutputPort,
  portComparisonFunction,
  portKeyFunction,
} from './port'

export const Actions = {
  NODE_INTERNAL_ACTIONS: 'ACTION_FOR_NODE',
  NODE_INPUTS_CHANGED: 'NODE_INPUTS_CHANGED',
  NODE_INITIALIZATION: 'NODE_INITIALIZATION',
}

class Flow {
  @observable
  lastNodeId: number = 0

  @observable
  nodes = new Map<number, INode<any>>()

  @observable
  connections = new IdentifiableSet<Connection>()

  @observable
  selectedNodes: number[] = []

  nodeRegistry = new OneToOneMap<INode<any>, HTMLElement>(
    (k) => k.id(),
    (v) => v,
  )

  portRegistry = new OneToOneMap<IPort, HTMLElement>(
    (k) => k.id(),
    (v) => v,
  )

  constructor() {
    makeObservable(this)
  }

  @action
  selectNode = (node: INode<any>, multiSelect: boolean) => {
    const oldSelected = [...this.selectedNodes]

    const nodeId = node.id()
    if (multiSelect) {
      if (this.selectedNodes.includes(nodeId)) {
        this.selectedNodes = this.selectedNodes.filter((nid) => nid !== nodeId)
      } else {
        this.selectedNodes = [...this.selectedNodes, nodeId]
      }
    } else {
      if (
        !this.selectedNodes.includes(nodeId) ||
        this.selectedNodes.length > 1
      ) {
        this.selectedNodes = [nodeId]
      }
    }

    oldSelected.forEach((id) => {
      this.nodes.get(id)!.selected = false
    })
    this.selectedNodes.forEach((id) => {
      this.nodes.get(id)!.selected = true
    })
  }

  @action
  addNode = (def: NodeDefination, position: Vector2d) => {
    const nodeId = this.lastNodeId++
    const node = def.nodeCreator(nodeId)
    node.position = position
    this.nodes.set(nodeId, node)
  }

  canAddConnection = (con: Connection): string | undefined => {
    const alreadyExists = this.connections.some((c) => c.id() === con.id())
    if (alreadyExists) {
      return 'Connection already exists!'
    }

    const endpoint = con.to
    if (
      endpoint.structure === 'single' &&
      this.connections.filter(
        (c) => c.to.node.id() === con.to.node.id() && c.to.name === con.to.name,
      ).length > 0
    ) {
      return 'Endpoint does not accepts multiple connections.'
    }

    const canCreateCyclic = this.checkForCyclicConnection(con)
    if (canCreateCyclic) {
      return 'Connection will create cyclic dependency'
    }
  }

  checkForCyclicConnection = ({ from, to }: Connection) => {
    const checkCyclic = (lastEndpoint: IPort): boolean => {
      const outputConnections = this.getOutgoingConnections(from)

      let isCyclic = false
      for (const oc of outputConnections) {
        if (oc.to.node.id() === from.node.id()) {
          return true
        } else {
          isCyclic = isCyclic || checkCyclic(oc.to)
        }
      }
      return isCyclic
    }
    return checkCyclic(to)
  }

  @action
  executeFlow = (node: INode<any>) => {
    const allAffectedPorts = node.inputs
    allAffectedPorts.forEach((p) => {
      const ipIsArray = p.structure === 'array'
      let inputValue: any
      if (ipIsArray) {
        inputValue = []
      }
      const incommingPorts = this.getIncommingConnection(p).map((c) => c.from)
      incommingPorts.forEach((port) => {
        const value = port.value
        if (Array.isArray(value) && port.structure === 'array' && ipIsArray) {
          value.forEach((v) => {
            inputValue.push(v)
          })
        } else if (ipIsArray) {
          inputValue.push(value)
        } else {
          inputValue = value
        }
      })
      p.node.propagateActionToFlow({ type: Actions.NODE_INPUTS_CHANGED })
    })
  }

  @action
  handleNodeInternalAction = (node: INode<any>, action: Action) => {
    const oldOutputs = node.outputs
    const { outputs: newOutputs, state } = node.reduceState(action)
    node.outputs = newOutputs
    node.state = state

    const diff = Diff.createDiff(
      oldOutputs,
      newOutputs,
      portKeyFunction,
      portComparisonFunction,
    )

    const removedOutputs = diff.exited()
    const updatedOutputs = diff.updated()

    //Manage connections
    const removedConnections = removedOutputs.flatMap((port) =>
      this.getOutgoingConnections(port),
    )

    const updatedConnections = updatedOutputs.flatMap((port) =>
      this.getOutgoingConnections(port),
    )

    const affectedPortsByRemoval = removedConnections.map((c) => c.to)
    const affectedPortByUpdate = updatedConnections.map((c) => c.to)

    //Remove connections from removed port
    this.connections = IdentifiableSet.from(
      this.connections.filter((c) => !removedConnections.includes(c)),
    )

    //Update from port value
    updatedOutputs.forEach((op) => {
      const connections = this.getOutgoingConnections(op)
      connections.forEach((con) => {
        con.from = op
      })
    })

    const allAffectedPorts: InputPort[] = [
      ...affectedPortByUpdate,
      ...affectedPortsByRemoval,
    ]
    allAffectedPorts.forEach((p) => {
      this.executeFlow(p.node)
    })
  }

  getOutgoingConnections = (port: OutputPort) => {
    return this.connections.filter((con) => con.from.id() === port.id())
  }

  getIncommingConnection = (port: InputPort) => {
    return this.connections.filter((con) => con.to.id() === port.id())
  }

  @action
  reduceActions = (node: INode<any>, action: Action) => {
    if (action.type === Actions.NODE_INTERNAL_ACTIONS) {
      this.handleNodeInternalAction(node, action.action)
    }
  }

  registerPort = (port: IPort, element: HTMLElement) => {
    this.portRegistry.set(port, element)
  }

  registerNode = (node: INode<any>, element: HTMLElement) => {
    this.nodeRegistry.set(node, element)
  }

  @action
  addConnection = (conn: Connection) => {
    const newConnections = IdentifiableSet.from(this.connections.values())
    newConnections.push(conn)
    this.connections = newConnections
    this.executeFlow(conn.to.node)
  }
}

const flow = new Flow()

export default flow
