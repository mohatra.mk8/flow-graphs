import { makeObservable, observable } from 'mobx'
import { Diff, KeyFunction, Predicate } from '../diff-selection'
import { IPortViewPlugin, PortViewPluginCreator } from '../port-view-plugin'
import { Identifiable } from './identifiable'
import { INode } from './node'

export type PortType = 'input' | 'output'
export type PortStructure = 'single' | 'array'

export abstract class IPort implements Identifiable {
  @observable
  node: INode<any>

  @observable
  name: string

  @observable
  type: PortType

  @observable
  structure: PortStructure

  @observable
  viewPlugin?: IPortViewPlugin

  @observable
  value?: any

  constructor(
    node: INode<any>,
    name: string,
    type: PortType,
    structure: PortStructure,
    viewPluginCreator?: PortViewPluginCreator,
  ) {
    this.node = node
    this.name = name
    this.type = type
    this.structure = structure

    if (viewPluginCreator) {
      this.viewPlugin = viewPluginCreator(this)
    }
    makeObservable(this)
  }

  id = () => {
    return `nodeId ${this.node.id()} name ${this.name} type ${
      this.type
    } structure ${this.structure}`
  }
}

export class InputPort extends IPort {
  constructor(node: INode<any>, name: string, structure: PortStructure) {
    super(node, name, 'input', structure)
  }
}

export class OutputPort extends IPort {
  constructor(node: INode<any>, name: string, structure: PortStructure) {
    super(node, name, 'output', structure)
  }
}

export const portComparisonFunction: Predicate<IPort> = (
  p1: IPort,
  p2: IPort,
) => {
  if (p1.id() !== p2.id()) {
    return false
  }

  if (p1 === p2) {
    return true
  }

  if (p1.value === p2.value) {
    return true
  }

  if (Array.isArray(p1.value) && Array.isArray(p2.value)) {
    const arrayDiff = Diff.createDiff(
      p1.value,
      p2.value,
      (a) => a,
      (a, b) => a === b,
    )
    return arrayDiff.noChange().length === p1.value.length
  }

  return p1.value === p2.value
}

export const portKeyFunction: KeyFunction<IPort> = (port) => port.id()
