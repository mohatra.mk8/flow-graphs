import { observable } from 'mobx'
import { KeyFunction, Predicate } from '../diff-selection'
import { Identifiable } from './identifiable'
import { InputPort, OutputPort, portComparisonFunction } from './port'

export class Connection implements Identifiable {
  @observable
  from: OutputPort

  @observable
  to: InputPort

  constructor(from: OutputPort, to: InputPort) {
    this.from = from
    this.to = to
  }

  id = () => {
    return `from ${this.from.id()} to ${this.to.id()}`
  }
}

export const connectionKeyFunction: KeyFunction<Connection> = (con) => con.id()
export const connectionComparisonFunction: Predicate<Connection> = (
  con1,
  con2,
) => {
  return (
    connectionKeyFunction(con1) === connectionKeyFunction(con2) &&
    portComparisonFunction(con1.from, con2.from) &&
    portComparisonFunction(con1.to, con2.to)
  )
}
