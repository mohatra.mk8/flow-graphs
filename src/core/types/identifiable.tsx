export type Identifier = string | number

export interface Identifiable {
  id: () => Identifier
}
