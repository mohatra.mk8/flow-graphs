import { makeObservable, observable } from 'mobx'
import { Vector2d } from '../data-structure'
import { INodeViewPlugin } from '../node-view-plugin'
import flow, { Actions } from './flow'
import { Identifiable, Identifier } from './identifiable'
import { InputPort, OutputPort } from './port'

export interface Action {
  type: string
  [key: string]: any
}

export type NodeBodyType = 'full-body' | 'header-only'
export type NodeCreator = (id: number) => INode<any>

export abstract class INode<T> implements Identifiable {
  @observable nodeId: number
  @observable position: Vector2d = new Vector2d()

  @observable inputs: InputPort[] = []
  @observable outputs: OutputPort[] = []
  @observable state?: T | undefined
  @observable bodyType: NodeBodyType
  @observable name: string
  @observable nodeViewPlugins = new Map<Identifier, INodeViewPlugin>()
  @observable selected: boolean = false

  constructor(nodeId: number, name: string, bodyType: NodeBodyType) {
    this.nodeId = nodeId
    this.name = name
    this.bodyType = bodyType
    makeObservable(this)
  }

  abstract reduceState(action: Action): { outputs: OutputPort[]; state: any }

  id = () => {
    return this.nodeId
  }

  selectMe = (multiSelect: boolean) => {
    flow.selectNode(this, multiSelect)
  }

  propagateActionToFlow = (action: Action) => {
    flow.reduceActions(this, {
      type: Actions.NODE_INTERNAL_ACTIONS,
      action: action,
    })
  }
}
