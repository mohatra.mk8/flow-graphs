import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { Col } from 'react-bootstrap'
import { useDrag } from 'react-dnd'
import {
  NodeDefination,
  NodesTree,
  searchNodes,
} from '../../core/node-registry'
import { useDebouncer } from '../../hooks/useDebouncer'
import { DropItemTypes } from '../FlowGraphEditor/FlowGraphEditor'
import { TreeView } from '../TreeView/TreeView'
import './LeftPane.scss'

export const LeftPane = observer(() => {
  const [searchFilter, setSearhFilter] = useState<string>('')
  const [setSearchText] = useDebouncer(
    (text: string) => setSearhFilter(text),
    500,
  )

  return (
    <Col xl={2} lg={2} md={2} sm={3} xs={4} className="left-pane fg-card">
      <div className="fg-card-header search-box">
        <input
          type="text"
          placeholder="Search node..."
          onChange={(e) => {
            setSearchText(e.target.value)
          }}
        />
      </div>
      {getNodesList(searchNodes(searchFilter))}
      <div className="left-pane-menu fg-card-footer"></div>
    </Col>
  )
})

const getNodesList = (tree: NodesTree) => {
  const treeMaker = (t1: NodesTree) => {
    const keys = Object.keys(t1)
    return keys.map((k) => {
      const value = t1[k]
      if (value instanceof NodeDefination) {
        return (
          <TreeView
            key={k}
            name={''}
            display={
              <NodeDisplay
                name={value.name}
                group={value.group}
                icon={value.icon}
              />
            }
            rowClassName="node-row"
          />
        )
      }
      return (
        <TreeView key={k} name={k}>
          {treeMaker(value as NodesTree)}{' '}
        </TreeView>
      )
    })
  }
  return <div className="node-list-container p-2">{treeMaker(tree)}</div>
}

const NodeDisplay = ({ name, group, icon }: any) => {
  const [, dragRef] = useDrag(() => ({
    type: DropItemTypes.NODE,
    item: { group: group, name: name },
  }))
  return (
    <span ref={dragRef}>
      {icon}
      {<span style={{ marginLeft: 4 }}>{name}</span>}{' '}
    </span>
  )
}
