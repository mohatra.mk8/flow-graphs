import { observer } from 'mobx-react-lite'
import React from 'react'
import { FlowGraphEditor } from '../FlowGraphEditor/FlowGraphEditor'
import { LeftPane } from '../LeftPane/LeftPane'
import './FlowGraphs.scss'
import { FlowMenu } from './FlowMenu'

export const FlowGraphs = observer(() => {
  return (
    <div className="flow-graphs h-100 w-100">
      <FlowMenu />
      <FlowGraphEditor>
        <LeftPane />
      </FlowGraphEditor>
    </div>
  )
})
