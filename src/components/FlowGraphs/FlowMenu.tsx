import { observer } from 'mobx-react-lite'
import React from 'react'
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'

export const FlowMenu = observer(() => {
  return (
    <Navbar bg="dark" expand="md" variant="dark" className="p-2">
      <Navbar.Brand href="#">
        <b>Flow</b>
        <b className="text-success" style={{ color: '#defff0' }}>
          Graphs
        </b>{' '}
        0.0.1
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <NavDropdown title="Menu" id="basic-nav-dropdown">
            <NavDropdown.Item onClick={() => null}>Save</NavDropdown.Item>
            <NavDropdown.Item onClick={() => null}>Open</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item onClick={() => null}>
              Export render
            </NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
})
