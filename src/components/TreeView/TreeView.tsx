import React, { useState } from 'react'
import { BsDashSquare, BsPlusSquare } from 'react-icons/bs'
import { animated, config, useSpring } from 'react-spring'
import './TreeView.scss'

export type TreeViewProps = {
  display?: JSX.Element
  name: string

  lineColor?: string
  lineWidth?: string

  rowStyle?: React.CSSProperties
  rowClassName?: string

  children?: any

  open?: boolean

  ref?: any

  onClick?: (this: any, event: any) => any
}

export const TreeView = (props: TreeViewProps) => {
  const [open, setOpen] = useState<boolean>(props.open || true)

  const borderStyle: React.CSSProperties = {
    borderLeft:
      props.children && open
        ? `${props.lineWidth || '1px'} dotted ${props.lineColor || 'black'}`
        : 'none',
  }

  const animation = useSpring({
    from: {
      height: open ? 0 : 'auto',
      opacity: open ? 0 : 1,
      transform: open ? 'translate3d(0px,0,0)' : 'translate3d(6px,0,0)',
    },
    to: {
      height: open ? 'auto' : 0,
      opacity: open ? 1 : 0,
      transform: !open ? 'translate3d(0px,0,0)' : 'translate3d(6px,0,0)',
    },

    config: {
      ...config.default,
      restVelocity: 1,
    },
  })

  const toggle = () => {
    setOpen((open) => !open)
  }

  return (
    <div className={'treeview'}>
      <span
        style={{ ...props.rowStyle, position: 'relative' }}
        className={props.rowClassName}
        ref={props.ref}
        onClick={props.onClick}
      >
        {props.children ? (
          open ? (
            <span onClick={toggle}>
              <BsDashSquare /> {props.display && props.display}
            </span>
          ) : (
            <span onClick={toggle}>
              <BsPlusSquare /> {props.display && props.display}
            </span>
          )
        ) : props.display ? (
          props.display
        ) : (
          ''
        )}
        <span style={{ marginLeft: 4 }}>{props.name}</span>
      </span>
      {
        <animated.div
          className="animated-div"
          style={{ ...borderStyle, ...animation }}
        >
          {props.children}
        </animated.div>
      }
    </div>
  )
}
