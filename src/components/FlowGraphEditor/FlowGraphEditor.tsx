import { observer } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import { useDrop } from 'react-dnd'
import { useToasts } from 'react-toast-notifications'
import {
  clientToRelativePosition,
  cubicBezierPath,
  getElementOffset,
  getMatrixTranslateValues,
  pageToRelativePosition,
  SVG_XML_NS,
} from '../../core/commons'
import { MouseButton } from '../../core/mouse-buttons'
import { getNodeDefination } from '../../core/node-registry'
import { NodeRender } from '../../core/node-render'
import { Connection } from '../../core/types/connection'
import flow from '../../core/types/flow'
import { InputPort, OutputPort } from '../../core/types/port'
import { useReferenceInstances } from '../../hooks/useReferenceInstances'
import './FlowGraphEditor.scss'

const isEditor = (element: Element) => {
  return element && element.classList.contains('flow-graph-editor')
}

type PanInteraction = {
  panning: boolean
}

type ElementRegistry = {
  nodesContainer?: HTMLElement
  pathContainer?: SVGElement
}

type ConnectionCreationInteraction = {
  creatingConnection: boolean
  fromPortElement?: HTMLElement
  tempPath?: SVGPathElement
}

export const DropItemTypes = {
  NODE: 'NODE',
}

export const FlowGraphEditor = observer(({ children }: any) => {
  const toast = useToasts()
  const panInteraction = useReferenceInstances<PanInteraction>(() => ({
    panning: false,
  }))

  const elementRegistry = useReferenceInstances<ElementRegistry>(() => {
    return {}
  })

  const conCreationInteraction = useReferenceInstances<
    ConnectionCreationInteraction
  >(() => ({
    creatingConnection: false,
  }))

  const [, drop] = useDrop(() => ({
    accept: DropItemTypes.NODE,
    drop: ({ name, group }: any, monitor) => {
      const clientOffset = monitor.getClientOffset()!
      const element = document.elementFromPoint(clientOffset.x, clientOffset.y)
      if (element && isEditor(element)) {
        const position = clientToRelativePosition(
          {
            clientX: clientOffset.x,
            clientY: clientOffset.y,
          },
          elementRegistry.nodesContainer!,
        )
        const nodeDef = getNodeDefination({ name, group })!
        flow.addNode(nodeDef, position)
      }
    },
  }))

  // Panning
  useEffect(() => {
    const handlePanStart = (event: MouseEvent) => {
      if (event.button !== MouseButton.LEFT) return

      const element = document.elementFromPoint(event.clientX, event.clientY)
      if (element && isEditor(element)) {
        panInteraction.panning = true
      }
    }
    const handlePanning = (event: MouseEvent) => {
      if (panInteraction.panning && elementRegistry.nodesContainer) {
        event.preventDefault()
        const translate = getMatrixTranslateValues(
          elementRegistry.nodesContainer,
        )
        elementRegistry.nodesContainer.style.transform = `translate(${
          translate.x + event.movementX
        }px,${translate.y + event.movementY}px)`
        elementRegistry.pathContainer!.style.transform = elementRegistry.nodesContainer!.style.transform
      }
    }

    const handlePanEnd = (event: Event) => {
      panInteraction.panning = false
    }

    document.addEventListener('mousedown', handlePanStart)
    document.addEventListener('mousemove', handlePanning)
    document.addEventListener('mouseup', handlePanEnd)

    return () => {
      document.removeEventListener('mousedown', handlePanStart)
      document.removeEventListener('mousemove', handlePanning)
      document.removeEventListener('mouseup', handlePanEnd)
    }
  }, [panInteraction, elementRegistry])

  // Connection creation
  useEffect(() => {
    function handleConnectionCreationStart(event: MouseEvent) {
      if (event.button !== MouseButton.LEFT) return

      const element = document.elementFromPoint(event.clientX, event.clientY)
      if (
        element &&
        element.classList.contains('endpoint') &&
        element.classList.contains('output')
      ) {
        event.preventDefault()
        conCreationInteraction.creatingConnection = true
        const path = document.createElementNS(SVG_XML_NS, 'path')
        path.setAttribute('stroke', '#01b00a')
        path.setAttribute('stroke-width', '0.3rem')
        path.setAttribute('fill', 'none')
        path.setAttribute('line-cap', 'round')
        path.setAttribute('stroke-dasharray', '5,5,5')
        conCreationInteraction.fromPortElement = element as HTMLElement
        conCreationInteraction.tempPath = path
        elementRegistry.pathContainer!.append(path)
      }
    }

    function handleConnectionCreating(event: MouseEvent) {
      if (conCreationInteraction.creatingConnection) {
        event.preventDefault()
        const fromEndpoint = conCreationInteraction.fromPortElement!
        const domRect = fromEndpoint.getBoundingClientRect()
        const pagePosition = getElementOffset(fromEndpoint)
        const startPosition = pageToRelativePosition(
          {
            pageX: pagePosition.left + domRect.width / 2,
            pageY: pagePosition.top + domRect.height / 2,
          },
          elementRegistry.nodesContainer!,
        )

        const endPosition = pageToRelativePosition(
          event,
          elementRegistry.nodesContainer!,
        )

        conCreationInteraction.tempPath!.setAttribute(
          'd',
          cubicBezierPath(startPosition, endPosition),
        )
      }
    }

    function handleConnectionCreationStop(event: MouseEvent) {
      if (conCreationInteraction.creatingConnection) {
        const element = document.elementFromPoint(event.clientX, event.clientY)
        if (
          element &&
          element.classList.contains('endpoint') &&
          element.classList.contains('input')
        ) {
          const fromPort = flow.portRegistry.getKeyByValue(
            conCreationInteraction.fromPortElement as HTMLElement,
          )
          const toPort = flow.portRegistry.getKeyByValue(element as HTMLElement)
          const connection = new Connection(
            fromPort as InputPort,
            toPort as OutputPort,
          )

          const errorMessage = flow.canAddConnection(connection)
          if (errorMessage) {
            toast.addToast(errorMessage, {
              display: 'error',
              autoDismiss: true,
            })
          } else {
            flow.addConnection(connection)
          }
        }

        event.preventDefault()
        conCreationInteraction.creatingConnection = false
        elementRegistry.pathContainer!.removeChild(
          conCreationInteraction.tempPath!,
        )
        conCreationInteraction.fromPortElement = undefined
      }
    }

    document.addEventListener('mousedown', handleConnectionCreationStart)
    document.addEventListener('mousemove', handleConnectionCreating)
    document.addEventListener('mouseup', handleConnectionCreationStop)

    return () => {
      document.removeEventListener('mousedown', handleConnectionCreationStart)
      document.removeEventListener('mousemove', handleConnectionCreating)
      document.removeEventListener('mouseup', handleConnectionCreationStop)
    }
  }, [elementRegistry, conCreationInteraction, toast])

  return (
    <div className="flow-graph-editor blueprint-bg" ref={drop}>
      <svg
        style={{ overflow: 'visible' }}
        ref={(element) => {
          element && (elementRegistry.pathContainer = element)
        }}
      ></svg>
      <div
        className="nodes-container"
        ref={(element) => {
          element && (elementRegistry.nodesContainer = element)
        }}
      >
        {Array.from(flow.nodes.values()).map((node) => {
          return <NodeRender node={node} key={`node ${node.id()}`} />
        })}
      </div>

      {children}
    </div>
  )
})
