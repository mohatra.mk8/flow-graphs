import { useRef } from 'react'

export function useReferenceInstances<T>(constructFunction: () => T) {
  const ref = useRef<T>(constructFunction())
  return ref.current
}
