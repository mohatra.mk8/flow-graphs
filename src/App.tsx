import React from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { ToastProvider } from 'react-toast-notifications'
import './app.scss'
import { FlowGraphs } from './components/FlowGraphs/FlowGraphs'

function App() {
  return (
    <div className="app">
      <DndProvider backend={HTML5Backend}>
        <ToastProvider>
          <FlowGraphs />
        </ToastProvider>
      </DndProvider>
    </div>
  )
}

export default App
